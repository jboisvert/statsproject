# StatsProject
In my Statistics course, we were asked to create an application in C# about any of the topics covered in the course. 
I chose hypothesis testing using Central Limit theorem. The app can calculate a Z-Score (with and without the sample size), conduct a hypothesis test based on the given criteria,
and can also interpret and conduct a hypothesis test on a typed out question. The scope of the questions were based on my teacher's questions.

Please feel free to contribute or give your thoughts / comments / feedback about the project :)

