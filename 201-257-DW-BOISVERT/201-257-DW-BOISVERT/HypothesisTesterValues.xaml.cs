﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _201_257_DW_BOISVERT
{
    /// <summary>
    /// This page is used to conduct a hypothesis test if the user already knows the values
    /// </summary>
    public partial class HypothesisTesterValues : Page
    {
        //Will be assigned the actualy hypothesis test and used if the user wants detailed steps
        private StepsForHypothesisCLT stepsForProblem;

        /// <summary>
        /// Constructor and sets the help button invisible since the user has no use clicking it. 
        /// </summary>
        public HypothesisTesterValues()
        {
            InitializeComponent();
            HelpBtn.Visibility = Visibility.Hidden;
        }

        /// <summary>
        /// For when interperator button is clicked the interperter page is loaded.
        /// </summary>
        private void InterpreterBtn_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow.Content = new Interperter();
        }

        /// <summary>
        /// Loads the exact values page for the z score
        /// </summary>
        private void AlreadyHaveValuesBtn_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow.Content = new ExactValues();
        }

        /// <summary>
        /// For when interperator button is clicked the interperter page is loaded.
        /// </summary>
        private void AboutBtn_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow.Content = new About();

        }

        /// <summary>
        /// Loads the hypothesis tester where the user alreayd has values to enter 
        /// </summary>
        private void HypotehsisTesterBtn_Click_1(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow.Content = new HypothesisTesterValues();
        }

        /// <summary>
        /// Conducts hypothesis test only if the inputs are correct
        /// </summary>
        private void TestHypothesisBtn_Click(object sender, RoutedEventArgs e)
        {

            //Only conducts test if the inputs are correct
            if (InputsAreCorrect())
            {

                string muStr = MuTxtBox.Text;
                string sigmaStr = SigmaTxtBox.Text;
                string xBarStr = SampleMeanTxtBox.Text;
                string nStr = NumberTxtBox.Text;


                double mu = Convert.ToDouble(muStr);
                double sigma = Convert.ToDouble(sigmaStr);
                double xBar = Convert.ToDouble(xBarStr);
                int n = Convert.ToInt32(nStr);

                //Actual test is done
                try
                {

                    ZScore z = new ZScore(mu, sigma, xBar, n);

                    string aStr = AlphaTxtBox.Text;
                    double a = Convert.ToDouble(aStr);
                    char current = Convert.ToChar(EquaitySymbolCmb.Text);
                    try
                    {
                        CLTHypothesisTester test = new CLTHypothesisTester(z, a, current);

                        string result = test.TestClaim();

                        HypothesisResultLbl.Content = result;
                        this.stepsForProblem = new StepsForHypothesisCLT(z, test);
                        HelpBtn.Visibility = Visibility.Visible;

                    }
                    catch (ArgumentException ae)
                    {

                        ErrorLbl.Content = "Alpha cannot be more than 1!";
                        AlphaTxtBox.Background = Brushes.Red;

                    }

                }

                //In order to know if the n value is not large enough since ZScore class throws error
                catch (ArgumentException ae)
                {

                    ErrorLbl.Content = "N is not large enough";
                    NumberTxtBox.Background = Brushes.Red; 

                }


            }

           
        }



        /// <summary>
        /// Checks if inputs are correct
        /// </summary>
        private bool InputsAreCorrect()
        {

            bool blanks = AreAnyInputsBlank();

            //If true, there are blanks so inputs are not correct. 
            if (blanks)
            {

                HypothesisResultLbl.Content = "";
                return false;

            }

            //If any of the inputs are not a number will get caught here
            try
            {
                bool negatives = AreAnyInputsNegative();


                if (negatives)
                {
                    HypothesisResultLbl.Content = "";
                    return false;

                }

                //If none of the if statements were triggered above then all inputs are correct
                return true;


            }
            catch (FormatException c)
            {

                MuTxtBox.Background = Brushes.White;
                SigmaTxtBox.Background = Brushes.White;
                SampleMeanTxtBox.Background = Brushes.White;
                NumberTxtBox.Background = Brushes.White;
                AlphaTxtBox.Background = Brushes.White; 


                ErrorLbl.Content = "Can only enter numbers! Check inputs if any are words or not formatted correctly.";
                return false;
            }

        }

        /// <summary>
        /// Checks if any inputs are negative
        /// </summary>
        private bool AreAnyInputsNegative()
        {

            //Defaut is false since unless if is triggered it will remain false
            bool negatives = false;

            //Will be able to add error messages if more than one field is left blank
            string errorMessage = "";

            string muStr = MuTxtBox.Text;
            string sigmaStr = SigmaTxtBox.Text;
            string xBarStr = SampleMeanTxtBox.Text;
            string nStr = NumberTxtBox.Text;

            double mu = Convert.ToDouble(muStr);
            double sigma = Convert.ToDouble(sigmaStr);
            double xBar = Convert.ToDouble(xBarStr);
            double n = Convert.ToInt32(nStr);
            string aStr = AlphaTxtBox.Text;
            double a = Convert.ToDouble(aStr);

            //If Mu is negative
            if (mu < 0)
            {

                errorMessage = "Value needs to be positive!";
                MuTxtBox.Background = Brushes.Red;
                negatives = true;


            }
            else
            {

                MuTxtBox.Background = Brushes.White;


            }

            //If sigma is neagtive
            if (sigma < 0)
            {

                errorMessage = "Value needs to be positive!";
                SigmaTxtBox.Background = Brushes.Red;
                negatives = true;

            }
            else
            {

                SigmaTxtBox.Background = Brushes.White;


            }


            //If Sample mean is negative
            if (xBar < 0)
            {

                errorMessage = "Value needs to be positive!";
                SampleMeanTxtBox.Background = Brushes.Red;
                negatives = true;


            }
            else
            {

                SampleMeanTxtBox.Background = Brushes.White;

            }



            //If sample size is negative
            if (n < 0)
            {

                errorMessage = "Value needs to be positive!";
                NumberTxtBox.Background = Brushes.Red;
                negatives = true;

            }
            else
            {

                NumberTxtBox.Background = Brushes.White;

            }

            if (a < 0)
            {

                errorMessage = "Value needs to be positive!";
                AlphaTxtBox.Background = Brushes.Red;
                negatives = true;

            }
            else
            {

                AlphaTxtBox.Background = Brushes.White;

            }


            ErrorLbl.Content = errorMessage;
            return negatives;

        }

        /// <summary>
        /// Does the check to see if any of the inputs were left blank
        /// </summary>
        private bool AreAnyInputsBlank()
        {


            //Defaut is false since unless if is triggered it will remain false
            bool blanks = false;

            //Will be able to add error messages if more than one field is left blank
            string errorMessage = "";


            //If Mu left blank
            if ((MuTxtBox.Text).Equals(""))
            {

                errorMessage = "Cannot leave what is indicated in red blank";
                MuTxtBox.Background = Brushes.Red;
                blanks = true;


            }
            else
            {

                MuTxtBox.Background = Brushes.White;


            }

            //If sigma left blank 
            if ((SigmaTxtBox.Text).Equals(""))
            {

                errorMessage = "Cannot leave what is indicated in red blank";
                SigmaTxtBox.Background = Brushes.Red;
                blanks = true;

            }
            else
            {

                SigmaTxtBox.Background = Brushes.White;


            }


            //If Sample mean is blank 
            if ((SampleMeanTxtBox.Text).Equals(""))
            {

                errorMessage = "Cannot leave what is indicated in red blank";
                SampleMeanTxtBox.Background = Brushes.Red;
                blanks = true;


            }
            else
            {

                SampleMeanTxtBox.Background = Brushes.White;

            }

            if ((NumberTxtBox.Text).Equals(""))
            {

                errorMessage = "Cannot leave what is indicated in red blank";
                NumberTxtBox.Background = Brushes.Red;
                blanks = true;


            }
            else
            {

                NumberTxtBox.Background = Brushes.White;

            }

            if ((AlphaTxtBox.Text).Equals(""))
            {

                errorMessage = "Cannot leave what is indicated in red blank";
                AlphaTxtBox.Background = Brushes.Red;
                blanks = true;


            }
            else
            {

                AlphaTxtBox.Background = Brushes.White;

            }

            if (EquaitySymbolCmb.SelectedIndex == -1)
            {

                errorMessage += " Need to select equality.";
                EquaitySymbolCmb.Background = Brushes.Red;
                blanks = true;


            }
            else
            {
                //TODO make color of dropdown change
                BrushConverter converter = new BrushConverter();
                Brush customColor = (Brush)converter.ConvertFrom("#FFACACAC");
                EquaitySymbolCmb.Background = customColor;

            }


            ErrorLbl.Content = errorMessage;

            return blanks;

        }

        /// <summary>
        /// Used if user needs help understading their problem. 
        /// Pops open a new window with detailed steps
        /// </summary>
        private void HelpBtn_Click(object sender, RoutedEventArgs e)
        {
            if (this.stepsForProblem != null)
            {

                CLTExplainedWindow helpWindow = new CLTExplainedWindow(this.stepsForProblem);
                helpWindow.Show();

            }


        }
    }
}
