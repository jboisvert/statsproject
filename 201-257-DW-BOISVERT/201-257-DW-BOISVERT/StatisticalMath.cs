﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _201_257_DW_BOISVERT
{

    /// <summary>
    /// Holds methods that are useful for Statstics in the 201-257-DW Math 
    /// Course at Dawson College
    /// </summary>
    /// <remarks>
    /// Although none of these methods are used in the project, methods are still useful 
    /// in the future perhaps - (date of remark: 2018-11-25)
    /// </remarks>
    class StatisticalMath
    {
        /// <summary>
        /// Calculates a the mean (average) of a given double array repersenting a sample. 
        /// </summary>
        private static double CalculateMean(double[] sample)
        {

            double sumOfSample = 0;

            foreach (double aObservation in sample)
            {

                sumOfSample += aObservation;

            }

            double Mean = sumOfSample / sample.Length;
            return Mean;

        }

        /// <summary>
        /// Calculates a the median of a given double array repersenting a sample. 
        /// </summary>
        private static double CalculateMedian(double[] sample)
        {

            //Create copy of array to sort
            double[] sortedSample = new double[sample.Length];

            for (int i = 0; i < sample.Length; i++)
            {

                sortedSample[i] = sample[i];

            }

            Array.Sort(sortedSample);

            //If size of sample is even then median location is the size / 2. If not even it is divding by 2 plus 1
            int locationOfMedian = (sortedSample.Length % 2 == 0) ? (sortedSample.Length / 2) : (sortedSample.Length / 2) + 1;

            double median = sortedSample[locationOfMedian];

            return median;

        }

    }
}
