﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _201_257_DW_BOISVERT
{
    /// <summary>
    /// Exact values page is used to calculate the zscore based on the given values
    /// </summary>
    public partial class ExactValues : Page
    {
        /// <summary>
        /// Exact values page constructor
        /// </summary>
        public ExactValues()
        {
            InitializeComponent();
        }

        /// <summary>
        /// For when interperator button is clicked the interperter page is loaded.
        /// </summary>
        private void InterpreterBtn_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow.Content = new Interperter();
        }

        /// <summary>
        /// Loads the exact values page for the z score
        /// </summary>
        private void AlreadyHaveValuesBtn_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow.Content = new ExactValues();
        }

        /// <summary>
        /// Calculates z-score based on inputs. Also checks if inputs are correct
        /// </summary>
        private void CalculateZScoreBtn_Click(object sender, RoutedEventArgs e)
        {
            //Only calculate z-score if inputs are correct.
            if (inputsAreCorrect())
            {
                string muStr = MuTxtBox.Text;
                string sigmaStr = SigmaTxtBox.Text;
                string xBarStr = SampleMeanTxtBox.Text;
                string nStr = NumberTxtBox.Text;


                double mu = Convert.ToDouble(muStr);
                double sigma = Convert.ToDouble(sigmaStr);
                double xBar = Convert.ToDouble(xBarStr);

                //Sometimes n can be blank and other times it can have a value. 
                //But if n is entered it must be larger than 30. 
                //Therefore error thrown in ZScore class is thrown when n is not large enough
                //and is caught below. 
                if (nStr.Equals(""))
                {

                    ZScore z = new ZScore(mu, sigma, xBar);

                    double zScore = z.calculateZScore();

                    ZScoreLbl.Content = "Z-Score: " + Math.Round(zScore, 4).ToString();

                }
                else
                {

                    int n = Convert.ToInt32(nStr);
                    try
                    {

                        ZScore z = new ZScore(mu, sigma, xBar, n);

                        double zScore = z.calculateZScore();

                        ZScoreLbl.Content = "Z-Score: " + Math.Round(zScore,4).ToString();

                    }
                    catch (ArgumentException ae)
                    {

                        NumberTxtBox.Background = Brushes.Red;
                        ZScoreLbl.Content = "";
                        ErrorLbl.Content = "N needs to be larger than 30";

                    }

                }

            }
        }

        /// <summary>
        /// Checks if inputs are correct
        /// </summary>
        private bool inputsAreCorrect()
        {

            bool blanks = areAnyInputsBlank();

            //If true, there are blanks so inputs are not correct. 
            if (blanks)
            {

                ZScoreLbl.Content = "";
                return false; 

            }

            //If any of the inputs are not a number will get caught here
            try
            {
                bool negatives = areAnyInputsNegative();


                if (negatives)
                {
                    ZScoreLbl.Content = "";
                    return false;

                }

                //If none of the if statements were triggered above then all inputs are correct
                return true;


            }
            //Used for if a user enters something like "hi" into one of the inputs.
            catch (FormatException c)
            {

                //Just making them all white for testing
                MuTxtBox.Background = Brushes.White;
                SigmaTxtBox.Background = Brushes.White;
                SampleMeanTxtBox.Background = Brushes.White;
                NumberTxtBox.Background = Brushes.White;

                ErrorLbl.Content = "Can only enter numbers! Check inputs if any are words or not formatted correctly.";
                return false; 
            }

        }

        /// <summary>
        /// Checks if any inputs are negative
        /// </summary>
        private bool areAnyInputsNegative()
        {

            //Defaut is false since unless if is triggered it will remain false
            bool negatives = false;

            //Will be able to add error messages if more than one field is left blank
            string errorMessage = "";

            string muStr = MuTxtBox.Text;
            string sigmaStr = SigmaTxtBox.Text;
            string xBarStr = SampleMeanTxtBox.Text;
            string nStr = NumberTxtBox.Text;

            double mu = Convert.ToDouble(muStr);
            double sigma = Convert.ToDouble(sigmaStr);
            double xBar = Convert.ToDouble(xBarStr);

            //If n is blank then assign 0 if not assign value stored in it. 
            double n = (nStr.Equals("")) ? 0 : Convert.ToInt32(nStr);



            //If Mu is negative
            if (mu < 0)
            {

                errorMessage = "Value needs to be positive!";
                MuTxtBox.Background = Brushes.Red;
                negatives = true;


            }
            else
            {

                MuTxtBox.Background = Brushes.White;


            }

            //If sigma is neagtive
            if (sigma < 0)
            {

                errorMessage = "Value needs to be positive!";
                SigmaTxtBox.Background = Brushes.Red;
                negatives = true;

            }
            else
            {

                SigmaTxtBox.Background = Brushes.White;


            }


            //If Sample mean is negative
            if (xBar < 0)
            {

                errorMessage = "Value needs to be positive!";
                SampleMeanTxtBox.Background = Brushes.Red;
                negatives = true;


            }
            else
            {

                SampleMeanTxtBox.Background = Brushes.White;

            }



            //If sample size is negative
             if (n < 0)
             {

                errorMessage = "Value needs to be positive!";
                NumberTxtBox.Background = Brushes.Red;
                 negatives = true;

             }
             else
            {

                NumberTxtBox.Background = Brushes.White;

            }

            ErrorLbl.Content = errorMessage;
            return negatives; 

        }

        /// <summary>
        /// Does the check to see if any of the inputs were left blank
        /// </summary>
        private bool areAnyInputsBlank()
        {

            //Defaut is false since unless if is triggered it will remain false
            bool blanks = false;

            //Will be able to add error messages if more than one field is left blank
            string errorMessage = "";


            //If Mu left blank
            if ((MuTxtBox.Text).Equals(""))
            {

                errorMessage = "Cannot leave what is indicated in red blank";
                MuTxtBox.Background = Brushes.Red;
                blanks = true;


            }
            else
            {

                MuTxtBox.Background = Brushes.White;

            }

            //If sigma left blank 
            if ((SigmaTxtBox.Text).Equals(""))
            {

                errorMessage = "Cannot leave what is indicated in red blank";
                SigmaTxtBox.Background = Brushes.Red;
                blanks = true;

            }
            else
            {

                SigmaTxtBox.Background = Brushes.White;


            }


            //If Sample mean is blank 
            if ((SampleMeanTxtBox.Text).Equals(""))
            {

                errorMessage = "Cannot leave what is indicated in red blank";
                SampleMeanTxtBox.Background = Brushes.Red;
                blanks = true;


            }
            else
            {

                SampleMeanTxtBox.Background = Brushes.White;

            }


            //If user decides to make everything bank after getting errors for something else for n
            //Ensures it will not be red unless negative
            NumberTxtBox.Background = Brushes.White;


            ErrorLbl.Content = errorMessage;

            return blanks; 

        }

        /// <summary>
        /// For when interperator button is clicked the interperter page is loaded.
        /// </summary>
        private void AboutBtn_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow.Content = new About();

        }

        /// <summary>
        /// Loads the hypothesis tester where the user alreayd has values to enter 
        /// </summary>
        private void HypotehsisTesterBtn_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow.Content = new HypothesisTesterValues();
        }
    }
}
