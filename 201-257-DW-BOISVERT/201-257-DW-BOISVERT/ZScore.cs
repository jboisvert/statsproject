﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _201_257_DW_BOISVERT
{

    /// <summary>
    /// A ZScore object is used to create and calculate the z-score. 
    /// its properties are double mu, double sigma, double xBar, and int n
    /// </summary>
    public class ZScore
    {
        //Attributes
        private double mu;
        private double sigma;
        private double xBar;
        private int n;

        /// <summary>
        /// Creates a z-score object including no n 
        /// </summary>
        public ZScore(double mu, double sigma, double xBar)
        {

            this.mu = mu;
            this.sigma = sigma;
            this.xBar = xBar;

            //There is no need for n
            this.n = 0;

        }

        /// <summary>
        /// Getter for mu property 
        /// </summary>
        public double Mu { get { return this.mu; }  }

        /// <summary>
        /// Getter for sigma property 
        /// </summary>
        public double Sigma { get { return this.sigma; } }

        /// <summary>
        /// Getter for xBar property 
        /// </summary>
        public double XBar { get { return this.xBar; } }

        /// <summary>
        /// Getter for n property 
        /// </summary>
        public int N { get { return this.n; } }

        /// <summary>
        /// Creates a z-score object including a specified n
        /// </summary>
        public ZScore(double mu, double sigma, double xBar, int n)
        {

            this.mu = mu;
            this.sigma = sigma;
            this.xBar = xBar;

            if (isNLargeEnough(n))
            {

                this.n = n;

            }
            else
            {

                throw new ArgumentException("N is not large enough! Need to be more than 30!");

            }

        }

        /// <summary>
        /// Checks if n is larger than 30 for good approximation
        /// </summary>
        private static bool isNLargeEnough(int n)
        {


            if (n > 30)
            {

                return true;

            }

            //can only get here if the if stataent did not happen
            return false; 

        }

        /// <summary>
        /// Calculates the zScore of the object
        /// </summary>
        /// <remarks>
        /// To calculate z score : x̅ - µ / (σ / √n)
        /// </remarks>
        public double calculateZScore()
        {

            double numerator = this.xBar - this.mu;


            double denomenator = 0; 

            if (this.n == 0){

                denomenator = this.sigma;

            }
            else
            {

               denomenator = this.sigma / (Math.Sqrt((long)this.n));


            }


            double zScore = numerator / denomenator;

            return zScore; 

        }

        /// <summary>
        /// Just clones the original object and creates a new object with same attrobutes
        /// </summary>
        public ZScore Clone()
        {

            return new ZScore(this.mu, this.sigma, this.xBar, this.n);

        }

    }
}
