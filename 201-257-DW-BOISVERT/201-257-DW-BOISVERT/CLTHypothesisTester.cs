﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _201_257_DW_BOISVERT
{

    /// <summary>
    /// A CLTHypotheisTester object is used to conduct Centeral Limit Theorem based hypothesis tests.
    /// </summary>
    public class CLTHypothesisTester
    {

        //Holds the csv file containing the table of the areas under the standard normal curve
        private double[][] areasUnderTheStandardNormalCurve;

        //Holds the Z-score of the Ho to be used in the testing of the Claim
        private readonly ZScore zScoreOfClaim;

        //Holds the Z-score of the Ha (alpha aka rejection zone)
        private readonly double a;

        //Holds the Ho for testing of the claim 
        private readonly char equalityUsed;

        //Used to hold the z score of alpha
        private readonly double aZscore;


        //Below is just properties for the object
        public double[][] AreaUnderTheStandardNormalCurve { get;  }
        public ZScore ZScoreOfClaim { get { return this.zScoreOfClaim; } }

        public double A { get { return this.a; }  }

        public char EqualityUsed { get { return this.equalityUsed; }  }

        public double AZscore { get { return this.aZscore; } }

        /// <summary>
        /// A constructor that takes the zscore of the claim and a value for alpha and which equality is used for the claim
        /// </summary>
        public CLTHypothesisTester(ZScore zScoreOfClaim, double a, char equalityUsed)
        {

            //Alpha cannot be larger than 1. 
            if (a > 1)
            {

                throw new ArgumentException("Alpha cannot be bigger than 1!");


            }

            //Alpha cannot be negative
            if (a < 0)
            {

                throw new ArgumentException("Alpha cannot be negative!");


            }

            //Clone the zscore since it is a refernce type. 
            this.zScoreOfClaim = zScoreOfClaim.Clone();

            this.equalityUsed = equalityUsed;

            this.a = a;

            //If the Ho equality is an '=' or an '≠' then the rejection zone is two sided (a rejection zone for positive and negative)
            double alphaUsed = (this.equalityUsed == '=' || this.equalityUsed == '≠') ? this.a / 2 : a;

            double areaOfAlpha = 0.5 - alphaUsed;

            //Loads the areas under the normal curve in order to find the z score of alpha
            LoadAreas();

            //Determines the z score of alpha based on area of alpha
            double returnedZScore = GetZScoreFromArea(areaOfAlpha);

            //If the returned z score is -1 means there is an error reading the zscore of alpha
            if (returnedZScore == -1)
            {

                throw new ArgumentException("Zscore of alpha is -1 which means something went wrong in the reading");


            }

            //Only get here if if statement above does not happen. 
            this.aZscore = returnedZScore;

        }

        /// <summary>
        /// A method that finds out of the claim of the CLT test is true or not. 
        /// </summary>
        public string TestClaim()
        {

            double zScoreOfAlpha = this.aZscore;

            //Will be changed through the witch statment
            string result = "";


            //Finds out what to do based on the equality being used in the claim
            //Helps find out the result of the test
            switch (this.equalityUsed)
            {
                case '=':
                    {
                        if (this.zScoreOfClaim.calculateZScore() <= zScoreOfAlpha &&
                        this.zScoreOfClaim.calculateZScore() >= (zScoreOfAlpha * -1))
                        {

                            result = "Ho is accepted. Claim is true.";

                        }
                        else
                        {

                            result = "Ho is not accepted. Claim is false.";

                        }


                    } break;

                case '≠':
                    {
                        if (this.zScoreOfClaim.calculateZScore() > zScoreOfAlpha ||
                        this.zScoreOfClaim.calculateZScore() < (zScoreOfAlpha * -1))
                        {

                            result = "Ho is rejected. Claim is true.";

                        }
                        else
                        {

                            result = "Ho is accepted. Claim is false.";

                        }


                    }
                    break;


                case '<':
                    {
                        if (this.zScoreOfClaim.calculateZScore() >= zScoreOfAlpha)
                        {

                            result = "Ho is accepted. Claim is false.";

                        }
                        else
                        {

                            result = "Ho is not accepted. Claim is true.";

                        }


                    }
                    break;

                case '>':
                    {
                        if (this.zScoreOfClaim.calculateZScore() <= zScoreOfAlpha)
                        {

                            result = "Ho is accepted. Claim is false.";

                        }
                        else
                        {

                            result = "Ho is not accepted. Claim is true.";

                        }


                    }
                    break;

                case '≤':
                    {
                        if (this.zScoreOfClaim.calculateZScore() <= zScoreOfAlpha)
                        {

                            result = "Ho is accepted. Claim is true.";

                        }
                        else
                        {

                            result = "Ho is not accepted. Claim is false.";

                        }


                    }
                    break;

                //Same as ≥ 
                default:
                    {
                        //Needs to be negative
                        if (this.zScoreOfClaim.calculateZScore() >= (-1 * zScoreOfAlpha))
                        {

                            result = "Ho is accepted. Claim is true.";

                        }
                        else
                        {

                            result = "Ho is not accepted. Claim is false.";

                        }


                    }
                    break;

            }

            return result;

        }

        /// <summary>
        /// Gets the z-score from the file based on the area given        
        /// </summary>
        private double GetZScoreFromArea(double area)
        {
            //Must round to nearest 4 decimal places since values in table are 4 decimal places
            double roundedArea = Math.Round(area, 4);

            //Default is -1 because if -1 is returned will throw an error
            double zScore = -1;

            //Default 1 since there will never be a difference higher than 1 
            //aka everything will be smaller than 1
            double smallestDifference = 1;

            //Just default values for the position of the z-score closest to the area given
            int rowOfSmallest = 0;
            int columnOfSmallest = 0;

            for (int i = 0; i <this.areasUnderTheStandardNormalCurve.Length; i ++)
            {
                double[] row = this.areasUnderTheStandardNormalCurve[i];

                for (int j = 1; j < row.Length; j++)
                {

                    if (row[j] == roundedArea)
                    {

                        //For example, row[0] holds 0.1 , i is always 1 higher than index in table
                        //then I want to add 0.02 to my 0.1 so I do 3-1 = 2 then 2 * 0.01 which gives me 
                        //my 0.02. This creates my z-score of 0.12
                        zScore = row[0] + ( (j - 1) * 0.01 );
                        return zScore; 

                    }
                    //Used to find the spot with the smallest difference to be able to use that as the z-score
                    //So, if 0.4805 is not found in the file I can still use 2.6 as my z score since 
                    //0.4805-0.4803 = 0.002 which is the smallest difference found in the file
                    else
                    {

                        //Only care about difference so I want the absolute value not negatives
                        double difference = Math.Abs(row[j] - roundedArea);

                        //If the difference is smaller than the current smallest difference update values and 
                        //note it's location
                        if (difference < smallestDifference)
                        {

                            smallestDifference = difference;
                            rowOfSmallest = i;
                            columnOfSmallest = j;

                        }

                    }

                }

            }

            //Only get here if exact value was never found
              double[] smallestRow = this.areasUnderTheStandardNormalCurve[rowOfSmallest];
              zScore = smallestRow[0] + ((columnOfSmallest - 1) * 0.01);
              return zScore;

        }

        /// <summary>
        /// To read and load csv file of areas under normal curve       
        /// </summary>
        private void LoadAreas()
        {

            this.areasUnderTheStandardNormalCurve = new double[51][];

            //Currently in the debug folder. Unable to find how to build into .exe
            string[] linesFromFile = File.ReadAllLines(@"areas_under_the_standard_normal_curve.csv");

                for (int i = 0; i < areasUnderTheStandardNormalCurve.Length; i++)
                {

                    this.areasUnderTheStandardNormalCurve[i] = new double[11];

                    for (int j = 0; j < this.areasUnderTheStandardNormalCurve[i].Length; j++)
                    {

                    string[] partsOfLine = linesFromFile[i].Split(',');

                    this.areasUnderTheStandardNormalCurve[i][j] = Convert.ToDouble(partsOfLine[j]);

                    }

                }

        }

    }
}
