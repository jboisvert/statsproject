﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace _201_257_DW_BOISVERT
{
    /// <summary>
    /// This window is just used so user can not have to reset their values and see and learn how to solve the 
    /// problem. This will load the page Steps for hypothesis test hence why it takes that object in
    /// the construtor. 
    /// </summary>
    public partial class CLTExplainedWindow : Window
    {

        public CLTExplainedWindow(StepsForHypothesisCLT stepsForProblem)
        {
            InitializeComponent();

            //Loads the problem given so percise steps can be shown. 
            this.Content = stepsForProblem;
        }
    }
}
