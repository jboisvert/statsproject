﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _201_257_DW_BOISVERT
{
    /// <summary>
    /// Interaction logic for Page1.xaml
    /// </summary>
    public partial class About : Page
    {
        public About()
        {
            InitializeComponent();
        }

        private void InterpreterBtn_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow.Content = new Interperter();
        }

        private void AlreadyHaveValuesBtn_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow.Content = new ExactValues();
        }

        private void AboutBtn_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow.Content = new About();
        }

        private void HypotehsisTesterBtn_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow.Content = new HypothesisTesterValues();
        }
    }
}
