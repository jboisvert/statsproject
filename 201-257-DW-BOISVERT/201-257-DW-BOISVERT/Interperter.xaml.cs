﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _201_257_DW_BOISVERT
{
    /// <summary>
    /// This page interperates a CLT problem given by the user and conducts a CLT test based on it
    /// </summary>
    /// <remarks>
    /// The problems are scoped to only Garry's problems in the 201-257-DW course given in Fall 2018 
    /// at Dawson College. I decided to name it the 'Garry Lok Chu Algorithm'.
    /// </remarks>
    public partial class Interperter : Page
    {
        //Will be assigned the actualy hypothesis test and used if the user wants detailed steps
        private StepsForHypothesisCLT stepsForProblem;

        //Problems hard coded in since there are errors reading file with special characters like sigma
        private readonly string[] exampleProblems =
        {

            "A nutritionist claims that the mean tuna consumption by a person in Canada is 3,1 pounds per year. A random sample of 60 people is selected. The mean tuna consumption by a person is 2,9 pounds per year. The standard deviation is known to be 0,94. pounds. Test the claim using a = 0,08.",
            "The government reports that the mean cost of raising a child from birth to age 2 is 10460$. A random sample of 900 children is selected and has a mean cost of 10345$. The population standard deviation is 1540$. Test the claim using a = 0,05.",
            "A tea drinker's society estimates that the mean consumption of tea by a person in Canada is more than 80 liters per year. In a random sample of 100 people, the mean consumption of tea is 80,1 liters per year. If σ = 2,67 conduct a hypothesis test where α = 0,07.",
            "A computer programmer claims that average run time of his program is 5 seconds. A random sample of 40 programs was run, averaging 5,3 seconds. The standard deviation is known to be 1,1 seconds. Use a = 0,1 to conduct a hypothesis test.",
            "A lightbulb manufacturer guarantees that the mean life of its light bulb is at least 745 hours. A random sample of 36 light bulbs has a sample mean of 740 hours. If σ = 60 hours, test the claim using a = 0,02.",
            "A journalist wrote that NBA players averaged at least 80 inches. A random sample of 36 players averaged a sample mean of 78,8 inches. If σ is known to be 3,2 inches test the claim using a = 0,01.",
            "The hospital announces that the mean age of its patients is more than 65 years. A random sample of 40 patients at a hospital has an average age of 68,9. If σ = 10,2 years. Conduct a hypothesis test. Use a = 0,005.",
            "It is believed that Dawson students average less than 40 minutes to travel to college. A random sample of 49 students average 37,5 minutes. σ is known to be 12 minutes. Conduct a hypothesis test if a = 0,1.",
            "A&W claims that the average cost of an order is at most 5,80$. A random sample of 65 orders had a sample mean of 6,30$. σ is known to be 2,10$. Test the claim using a = 0,05."

        };

        /// <summary>
        /// For when interperator button is clicked the interperter page is loaded.
        /// </summary>
        public Interperter()
        {
            InitializeComponent();
            HelpBtn.Visibility = Visibility.Hidden;
        }

        /// <summary>
        /// Loads the exact values page for the z score
        /// </summary>
        private void AlreadyHaveValuesBtn_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow.Content = new ExactValues();
        }

        /// <summary>
        /// For when interperator button is clicked the interperter page is loaded.
        /// </summary>
        private void InterpreterBtn_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow.Content = new Interperter();
        }

        /// <summary>
        /// For when interperator button is clicked the interperter page is loaded.
        /// </summary>
        private void AboutBtn_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow.Content = new About();

        }

        /// <summary>
        /// Loads the hypothesis tester where the user alreayd has values to enter 
        /// </summary>
        private void HypotehsisTesterBtn_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow.Content = new HypothesisTesterValues();
        }

        /// <summary>
        /// Used if user needs help understading their problem. 
        /// Pops open a new window with detailed steps
        /// </summary>
        private void HelpBtn_Click(object sender, RoutedEventArgs e)
        {
            if (this.stepsForProblem != null)
            {

                CLTExplainedWindow helpWindow = new CLTExplainedWindow(this.stepsForProblem);
                helpWindow.Show();

            }


        }

        /// <summary>
        /// Takes the input and either interprates it and conducts a CLT hypothesis test
        /// Or notifies the user that the problem is not regonized through the Garry Lok Chu Algorithm
        /// </summary>
        private void CheckHypothesisBtn_Click(object sender, RoutedEventArgs e)
        {

            //Getting input from user
            string input = WordProblemTxtBox.Text;

            //Split up the problem up into sentences (hence split by a period) 
            string[] sentences = input.Split('.');

            //To find any string containing numbers
            Regex numberChecker = new Regex("[0-9]");

            //In Garry's questions the claim is always in the first sentence
            string sentenceWithClaim = sentences[0];

            string[] firstSentenceBrokenUp = sentenceWithClaim.Split(' ');

            double mu = -1;
            int n = -1;
            double xbar = -1;
            double sigma = -1;
            double alpha = -1;
            char operatorOfClaim = ' ';

            for (int i = 0; i < firstSentenceBrokenUp.Length; i++)
            {
                //Garry's problems have the claim be = 
                //when he says .... is 80 ... in the first sentence
                if (firstSentenceBrokenUp[i].Equals("is") &&
                    numberChecker.IsMatch(firstSentenceBrokenUp[i + 1]))
                {

                    operatorOfClaim = '=';
                    mu = assignNumber(firstSentenceBrokenUp[i + 1]);
                }

                //Some of Garry's problems have the claim be > 
                //when he says .... is more than 80 ... in the first sentence
                if (
                    firstSentenceBrokenUp[i].Equals("more")  &&
                    firstSentenceBrokenUp[i + 1].Equals("than")  &&
                    numberChecker.IsMatch(firstSentenceBrokenUp[i + 2]))
                {

                    operatorOfClaim = '>';
                    mu = assignNumber(firstSentenceBrokenUp[i + 2]);
                }

                //Some of Garry's problems have the claim be ≤ 
                //when he says .... at most 80 ... in the first sentence
                if (
                    firstSentenceBrokenUp[i].Equals("at") &&
                    firstSentenceBrokenUp[i + 1].Equals("most") &&
                    numberChecker.IsMatch(firstSentenceBrokenUp[i + 2]))
                {

                    operatorOfClaim = '≤';
                    mu = assignNumber(firstSentenceBrokenUp[i + 2]);
                }

                //Garry's problems have the claim be < 
                //when he says .... is less than 80 ... in the first sentence
                if (
                    firstSentenceBrokenUp[i].Equals("less") &&
                    firstSentenceBrokenUp[i + 1].Equals("than") &&
                    numberChecker.IsMatch(firstSentenceBrokenUp[i + 2]))
                {

                    operatorOfClaim = '<';
                    mu = assignNumber(firstSentenceBrokenUp[i + 2]);
                }

                //Garry's problems have the claim be < 
                //when he says .... is at least 80 ... in the first sentence
                if (
                    firstSentenceBrokenUp[i].Equals("at") &&
                    firstSentenceBrokenUp[i + 1].Equals("least") &&
                    numberChecker.IsMatch(firstSentenceBrokenUp[i + 2]))
                { 

                    operatorOfClaim = '≥';
                    mu = assignNumber(firstSentenceBrokenUp[i + 2]);

                }

            }

            //Next find the sentence that talks about the random sample which then is followed by 
            //the sample mean
            for (int i = 1; i < sentences.Length; i++)
            {

                //This indicates that it is the sentence talking about the random sample
                if (sentences[i].Contains("random sample of"))
                {

                    string[] randomSampleSentence = sentences[i].Split(' ');

                    for (int j = 0; j < randomSampleSentence.Length; j++)
                    {

                        if (randomSampleSentence[j].Equals("random") &&
                            randomSampleSentence[j + 1].Equals("sample") &&
                            randomSampleSentence[j + 2].Equals("of") &&
                            numberChecker.IsMatch(randomSampleSentence[j + 3]))
                        {

                            n = Convert.ToInt32(randomSampleSentence[j + 3]);

                            //found n no need to continue loop
                            break;
                        }

                    }

                    //already found n no need to continue loop
                    break;

                }

            }

            //Now to find sample mean. Since every problem talks about some sort of average
            //more than once possibly this may appear as a problem.
            //However, in Garry's quesitons the claim is always in the first sentence
            //so the first sentence is now ignore so any sentence talking about 'mean'
            //'average' or any other word similar is the sample mean
            //This indicates that it is the sentence talking about the random sample
            for (int i = 1; i < sentences.Length; i++)
            {
                if (sentences[i].Contains("mean") || sentences[i].Contains("average") 
                    || sentences[i].Contains("averaged") || sentences[i].Contains("averaging"))
                {

                    string[] sampleMeanSentence = sentences[i].Split(' ');



                    for (int j = 0; j < sampleMeanSentence.Length; j++)
                    {

                        if (numberChecker.IsMatch(sampleMeanSentence[j]))
                        {

                            double numberFound = assignNumber(sampleMeanSentence[j]);

                            //If it is alpha Garry only mentions alpha like:
                            //'a = ...' therefore if 2 spaces before is 'a' or 'α' 
                            //then keep looking for the number
                            string isThisAlpha = sampleMeanSentence[j];

                            //Somtimes sample mean is given in same sentence as 
                            //sample size so if the number found is the same as n
                            //cotinue through the loop
                           if (numberFound != n && (!(isThisAlpha.Equals("α") || isThisAlpha.Equals("a"))) )
                           {

                                xbar = numberFound;

                                //No need to continue found the number needed. 
                               break;

                            }

                        }

                    }

                    //already found n no need to continue loop
                    break;

                }


            }


            //Now to find the sentence containing the standard deviation. 
            //It is either written as "σ = .." or "standard deviation is .."
            //Therefore, will find sentence containing σ or standard deviation
            //and then find the number the follows after that pattern.
            for (int i = 1; i < sentences.Length; i++)
            {
                if (sentences[i].Contains("σ =") || sentences[i].Contains("standard deviation")
                    || sentences[i].Contains("σ is known to be"))
                {

                    string[] standardDeviationSentence = sentences[i].Split(' ');

                    for (int j = 0; j < standardDeviationSentence.Length; j++)
                    {

                        if (numberChecker.IsMatch(standardDeviationSentence[j]))
                        {

                            double numberFound = assignNumber(standardDeviationSentence[j]);

                            //Somtimes sigma is in the same sentence as n and xbar so to ensure do not
                            //get it twice.
                            if (!(numberFound == n || numberFound == xbar))
                            {

                                sigma = numberFound;

                                //No need to continue found the number needed. 
                                break;

                            }

                        }

                    }

                    //already found n no need to continue loop
                    break;

                }


            }

            //Lastly we must find the alpha. alpha is always written in Garry's questions as 
            //"α =" or "a =" if user cannot locate how to make th α character. therefore 
            //all that needs to be done is find the number after the '='.
            for (int i = 1; i < sentences.Length; i++)
            {
                if (sentences[i].Contains("α =") || sentences[i].Contains("a ="))
                {

                    string[] alphaSentence = sentences[i].Split(' ');

                    for (int j = 0; j < alphaSentence.Length; j++)
                    {

                        if ( (alphaSentence[j].Equals("α") || alphaSentence[j].Equals("a") ) &&
                            alphaSentence[j + 1].Equals("=") &&
                             numberChecker.IsMatch(alphaSentence[j + 2]))
                        {

                            double numberFound = assignNumber(alphaSentence[j + 2]);


                                alpha = numberFound;

                                //No need to continue found the number needed. 
                                break;

                            

                        }

                    }

                    //already found n no need to continue loop
                    break;

                }


            }

            //-1 means it never got changed so it means one of the values were not found in the text which
            //means there was a problem understanding the problem
            if (mu == -1 || n == -1 || xbar == -1 || sigma == -1 || alpha == -1 || operatorOfClaim == ' ')
            {

                ResultLbl.Content = "Hmm something is not working. Check for typos " + '\n' + "and ensure this is a question by Garry Lok Chu.";
                this.stepsForProblem = null;
                HelpBtn.Visibility = Visibility.Hidden;

            }
            //Conduct test
            else {

                ZScore claim = new ZScore(mu, sigma, xbar, n);
                CLTHypothesisTester test = new CLTHypothesisTester(claim, alpha, operatorOfClaim);

                //Used if the user needs help with the problem
                this.stepsForProblem = new StepsForHypothesisCLT(claim, test);

                ResultLbl.Content = test.TestClaim();

                //Make the help button visible. 
                HelpBtn.Visibility = Visibility.Visible;
            }


        }

        /// <summary>
        /// Converts a number looking like 23,3 stored in a string
        /// to a double 23.3
        /// </summary>
        /// <remarks>
        /// This is done because numbers cannot have decimals as a '.' since I split the word problem up by 
        /// a '.' and the comma makes 23,3 become 233 and not 23.3 when stored in a double. 
        /// The solution is the count how many times the decimal must move over and convert to proper decimal number
        /// </remarks>
        private double assignNumber(string number)
        {

            if (number.Contains(','))
            {

                //Need to figure out where my comma is in the number
                int locationOfComma = number.IndexOf(',');

                //Need to know size of number by minus the comma
                int sizeOfNumber = number.Length - 1;

                //To ensure if I have a number like 312,12
                //to convert 31212 I just move the decimal place 2 times.
                //So, size of number is 5 and location of ',' is 3 
                //so 5-3 is 2 which is how many decimal places i need to move by
                int moveDecimal = sizeOfNumber - locationOfComma;

                string toMultiplyByStr = "0.";

                for (int i = 1; i < moveDecimal; i++)
                {
                    //add a zero after the decimal spot
                    toMultiplyByStr += "0";

                }

                //Add the one at the end
                toMultiplyByStr += "1";

                //Get the actual decimal number to apply the multiplication to
                double multiplier = Convert.ToDouble(toMultiplyByStr);

                //Remove anything in the string that is not a number. 
                //Ensures conversion works properly. 
                number = Regex.Replace(number, "[^0-9]", "");

                //Comma is ignored so creates a whole number
                //for example 3,1 becomes 31. 
                double muToAssign = Convert.ToDouble(number);

                //Now number is in decimal form. 
                //for example 31 become 3.1
                muToAssign = muToAssign * multiplier;

                return muToAssign;

            }

            //Remove anything in the string that is not a number. 
            //Ensures conversion works properly. 
            number = Regex.Replace(number, "[^0-9]", "");

            return Convert.ToDouble(number);

        }

        /// <summary>
        /// Used to get one of the alreayd know questions for the user to learn from
        /// </summary>
        /// <remarks>
        /// All already known problems have been tested with and are working as expected. 
        /// </remarks>
        private void RandomProblemBtn_Click(object sender, RoutedEventArgs e)
        {
            Random randomNumber = new Random();
            int index = randomNumber.Next(this.exampleProblems.Length);

            WordProblemTxtBox.Text = this.exampleProblems[index];

        }
    }

}
