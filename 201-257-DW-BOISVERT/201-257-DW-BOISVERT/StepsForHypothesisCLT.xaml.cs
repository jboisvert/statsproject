﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _201_257_DW_BOISVERT
{
    /// <summary>
    /// This page is mainly used to educate the user on their CLT problem given before
    /// </summary>
    public partial class StepsForHypothesisCLT : Page
    {
        //Holds the Z-score of the claim to be used in the testing of the Claim
        private readonly ZScore zScoreOfClaim;

        //Actual hypothesis test that was conducted prior to loading this page
        private readonly CLTHypothesisTester hypothesisTest;

        /// <summary>
        /// Used to construct the page. Takes previous z score of claim and the CLT hypothesis test conducted prior
        /// </summary>
        public StepsForHypothesisCLT(ZScore zScoreOfClaim, CLTHypothesisTester hypothesisTest)
        {
            InitializeComponent();
            this.zScoreOfClaim = zScoreOfClaim;
            this.hypothesisTest = hypothesisTest;
            DisplayResults();
        }

        /// <summary>
        /// Used to display the result of the test with valuable steps.
        /// </summary>
        private void DisplayResults()
        {
            //First display the first check that n is larger than 30
            //This is assumed to be correct since test was successful.
            NLbl.Content = this.zScoreOfClaim.N.ToString() + " > 30";

            char equalityUsed = hypothesisTest.EqualityUsed;

            //Finding out if Ho or Ha is the claim
            if (equalityUsed == '=')
            {

                HoContentLbl.Content = "µ = " + zScoreOfClaim.Mu.ToString() + " (Claim)";
                HaContentLbl.Content = "µ ≠ " + zScoreOfClaim.Mu.ToString();

                LeftRejection.Visibility = Visibility.Visible;
                RightRejection.Visibility = Visibility.Visible;

                RightRejectionZ.Content = this.hypothesisTest.AZscore.ToString();
                LeftRejectionZ.Content = (this.hypothesisTest.AZscore * -1).ToString();

                //To know where Ho is pointing to
                if (this.zScoreOfClaim.calculateZScore() <= this.hypothesisTest.AZscore && 
                    this.zScoreOfClaim.calculateZScore() >= this.hypothesisTest.AZscore * -1)
                {

                    ZPointer.X2 = 300;

                }
                else if (this.zScoreOfClaim.calculateZScore() > this.hypothesisTest.AZscore)
                {

                    ZPointer.X2 = 450;

                }
                else
                {

                    ZPointer.X2 = 175;

                }
            }
            if (equalityUsed == '≠')
            {

                HoContentLbl.Content = "µ = " + zScoreOfClaim.Mu.ToString();
                HaContentLbl.Content = "µ ≠ " + zScoreOfClaim.Mu.ToString() + " (Claim)";

                LeftRejection.Visibility = Visibility.Visible;
                RightRejection.Visibility = Visibility.Visible;

                RightRejectionZ.Content = this.hypothesisTest.AZscore.ToString();
                LeftRejectionZ.Content = (this.hypothesisTest.AZscore * -1).ToString();

                //To know where Ho is pointing to
                if (!(this.zScoreOfClaim.calculateZScore() <= this.hypothesisTest.AZscore ||
                 this.zScoreOfClaim.calculateZScore() >= this.hypothesisTest.AZscore * -1))
                {

                    ZPointer.X2 = 300;

                }
                else if (this.zScoreOfClaim.calculateZScore() > this.hypothesisTest.AZscore)
                {

                    ZPointer.X2 = 450;

                }
                else
                {

                    ZPointer.X2 = 175;

                }

            }
            if (equalityUsed == '≤')
            {

                HoContentLbl.Content = "µ ≤ " + zScoreOfClaim.Mu.ToString() + " (Claim)";
                HaContentLbl.Content = "µ > " + zScoreOfClaim.Mu.ToString();

                LeftRejection.Visibility = Visibility.Hidden;
                RightRejection.Visibility = Visibility.Visible;

                RightRejectionZ.Content = this.hypothesisTest.AZscore.ToString();
                LeftRejectionZ.Visibility = Visibility.Hidden;

                //To know where Ho is pointing to
                if (this.zScoreOfClaim.calculateZScore() <= this.hypothesisTest.AZscore)
                {

                    ZPointer.X2 = 300;

                }
                else
                {

                    ZPointer.X2 = 450;

                }

            }
            if (equalityUsed == '≥')
            {

                HoContentLbl.Content = "µ ≥ " + zScoreOfClaim.Mu.ToString() + " (Claim)";
                HaContentLbl.Content = "µ < " + zScoreOfClaim.Mu.ToString();

                LeftRejection.Visibility = Visibility.Visible;
                RightRejection.Visibility = Visibility.Hidden;

                RightRejectionZ.Visibility = Visibility.Hidden;
                LeftRejectionZ.Content = (this.hypothesisTest.AZscore * -1).ToString();

                //To know where Ho is pointing to
                if (this.zScoreOfClaim.calculateZScore() >= this.hypothesisTest.AZscore * -1)
                {

                    ZPointer.X2 = 300;

                }
                else
                {

                    ZPointer.X2 = 175;

                }

            }
            if (equalityUsed == '<')
            {

                HoContentLbl.Content = "µ ≥ " + zScoreOfClaim.Mu.ToString();
                HaContentLbl.Content = "µ < " + zScoreOfClaim.Mu.ToString() + " (Claim)";

                LeftRejection.Visibility = Visibility.Visible;
                RightRejection.Visibility = Visibility.Hidden;

                RightRejectionZ.Visibility = Visibility.Hidden;
                LeftRejectionZ.Content = (this.hypothesisTest.AZscore * -1).ToString();

                //To know where Ho is pointing to
                if (!(this.zScoreOfClaim.calculateZScore() <= this.hypothesisTest.AZscore))
                {

                    ZPointer.X2 = 300;

                }
                else
                {

                    ZPointer.X2 = 175;

                }

            }
            if (equalityUsed == '>')
            {

                HoContentLbl.Content = "µ ≤ " + zScoreOfClaim.Mu.ToString();
                HaContentLbl.Content = "µ > " + zScoreOfClaim.Mu.ToString() + " (Claim)";

                LeftRejection.Visibility = Visibility.Hidden;
                RightRejection.Visibility = Visibility.Visible;

                RightRejectionZ.Content = this.hypothesisTest.AZscore.ToString();
                LeftRejectionZ.Visibility = Visibility.Hidden;

                //To know where Ho is pointing to
                if (!(this.zScoreOfClaim.calculateZScore() >= this.hypothesisTest.AZscore * 1))
                {

                    ZPointer.X2 = 300;

                }
                else
                {

                    ZPointer.X2 = 175;

                }

            }

            //Displays what the z score and what alpha actually equal to from previous 
            //CLT test
            WhatZEqualsToLbl.Content = Math.Round(this.zScoreOfClaim.calculateZScore(), 4);
            WhataEqualsToLbl.Content = this.hypothesisTest.A.ToString();

            if (equalityUsed == '=' || equalityUsed == '≠')
            {
                string a = this.hypothesisTest.A.ToString();
                string aOver2 = (this.hypothesisTest.A / 2).ToString();
                string areaOfA = (0.5 - Math.Round((this.hypothesisTest.A/2), 4)).ToString();

                WhatZaEqualsTo.Content = a + "/2 = " +
                    aOver2 +
                    ", 0.5 - " + aOver2 + "=" +
                    areaOfA +
                    ", Za = " +
                    this.hypothesisTest.AZscore.ToString();

            }
            else
            {
                string a = this.hypothesisTest.A.ToString();
                string areaOfA = (0.5 - this.hypothesisTest.A).ToString();


                WhatZaEqualsTo.Content = "0.5 - " + a + "=" + 
                 areaOfA + ", Za = " +
                 this.hypothesisTest.AZscore.ToString();

            }

            //Display actual result
            ResultLbl.Content = this.hypothesisTest.TestClaim();
            

        }

    }
}
